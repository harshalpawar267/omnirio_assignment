class Currency
  @@currency = {}
  def initialize(name, price)
    @@currency[name] = price
  end
  
  def self.all
    @@currency
  end
end
