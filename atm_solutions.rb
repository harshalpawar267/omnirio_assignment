require_relative 'currency'

class ATMSolution
  def initiate_billing
    input = get_input
    # Validate input
    @amount = input.to_i if input.match(/\A[1-9]\d*\Z/) 
    if @amount != nil
      # calculate currency over input values
      calculate_currency(@amount)
    else 
      responce(400,'Bad request', {})
    end
  end
  
  private
    def get_input
      puts "YOUR_SERVICE?input_amount="
      response = gets.chomp
    end

    def calculate_currency(amount)
      currency = Currency.all
      amt = {}
      currency.map do |curr,value|
        if amount >= value
          amt[discributions(curr)] = amount / value
          amount = amount % value
          if amount == 0
            break
          end
        end
      end
      responce(200,'Ok', amt)
    end
    
    def responce(code, message, amt) 
      data = {
        'status'  => code,
        'message' => message,
        'Currency'    => amt
      }
      print data
    end

    def discributions(curr)
      case curr
      when 'Quarter'
        'quarters'
      when 'Dim'
        'dims'
      when 'Nickel'
        'nickels'
      when 'Penny'
        'pennies'  
      end
    end
end

begin
  Currency.new('Quarter', 25)
  Currency.new('Dim', 10)
  Currency.new('Nickel', 5)
  Currency.new('Penny', 1)

  atm_solutions = ATMSolution.new
  atm_solutions.initiate_billing
end
